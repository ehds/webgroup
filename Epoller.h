//
// Created by ehds on 6/2/19.
//

#ifndef WEB_EPOLLER_H
#define WEB_EPOLLER_H
#include <sys/epoll.h>
#include <errno.h>
class Epoller {
    public:
        Epoller() {}
        ~Epoller() {}

        struct Event : public epoll_event
        {
            Event() { events = EPOLLERR; data.u64 = 0; }
            Event(unsigned int type, void *magic,int fd)
            { data.u64 = 0; events = type; data.ptr = magic;data.fd=fd; }
        };

        int create()
        {
            if ((descriptor_ = epoll_create(1024)) == -1)
            return descriptor_;
        }

        void registerEvent(int fd, Event &event)
        {
            epoll_ctl(descriptor_, EPOLL_CTL_ADD, fd,
                          (epoll_event*)&event);

        }

        void modifyEvent(int fd, Event &event)
        {
//            if (epoll_ctl(descriptor_, EPOLL_CTL_ADD, fd,
//                          (epoll_event*)&event) == -1)
//            {
//                if (errno == EEXIST)
//                    errno = 0;
//            }
            epoll_ctl(descriptor_,EPOLL_CTL_MOD,fd,(epoll_event*)&event);
        }

        void unregisterEvent(int fd)
        {
           epoll_ctl(descriptor_, EPOLL_CTL_DEL, fd, nullptr);
        }

        int waitEvent(Event *events, int size, int msec)
        {
            return epoll_wait(descriptor_, events, size, msec);
        }

protected:
    int descriptor_;
};

#endif //WEB_EPOLLER_H
