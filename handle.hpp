#include "server_base.hpp"
#include <fstream>
using namespace std;
using namespace MyWeb;


template<typename SERVER_TYPE>
void start_server(SERVER_TYPE &server){

	server.resource["^/string/?$"]["POST"] = [](ostream& response, Request & request){
		stringstream ss;
		*request.content >> ss.rdbuf();
		string content = ss.str();
		response << "HTTP/1.1 200 OK\r\nContent-Length: " << content.length() << "\r\n\r\n" << content;
	};
		server.resource["^/info/?$"]["GET"] = [](ostream& response, Request& reques){
			stringstream content_stream;
			content_stream << "<h1>Request:</h1>";
			content_stream << reques.method << " " << reques.path << "HTTP/" << reques.http_version << "<br>";
			for (auto & header : reques.header){
				content_stream << header.first << ":" << header.second << "<br>";

			}
			content_stream.seekp(0, ios::end);
			response << "HTTP/1.1 200 OK\r\nContent-Length" << content_stream.tellp() << "\r\n\r\n" << content_stream.rdbuf();
		};


		server.resource["^/match/([0-9a-zA-Z]+)/?$"]["GET"] = [](ostream& response, Request& request) {
			string number = request.path_match[1];
			response << "HTTP/1.1 200 OK\r\nContent-Length: " << number.length() << "\r\n\r\n" << number;
		};

		//����Ĭ�ϵ�GET�������û������ƥ��ɹ�����������������ᱻ����
		//Ĭ��Ϊ/web/index.html

		server.default_resource["^/?(.*)$"]["GET"] = [](ostream& response, Request& request){
			string filename = "webroot/";
			string path = request.path_match[1];
			//��ֹʹ�á�..����hack
			//rfind �ҵ����һ������.��λ��
			size_t last_post = path.rfind(".");
			size_t current_pos = 0;
			size_t pos;
			while ((pos = path.find('.', current_pos)) != string::npos && pos != last_post){
				current_pos = pos;
				//����'.'���ַ�ȫ���Ƴ�ֻ�������һ��
				path.erase(pos, 1);
				last_post--;
			}
			filename += path;
			ifstream ifs;
			//�򵥵�ƽ̨�޹ص��ļ���Ŀ¼���
			if (filename.find('.') == string::npos){
                //filename += '/';
                filename += "index.html";
        }
        ifs.open(filename, ifstream::in);

        if (ifs){
            ifs.seekg(0, ios::end);//�Ƶ�������ĩβ
            size_t length = ifs.tellg();//����������Ĵ�С
            ifs.seekg(0, ios::beg);//�Ƶ��������Ŀ�ʼλ��
            response << "HTTP/1.1 200 OK\r\nContent-Length: " << length << "\r\n\r\n" << ifs.rdbuf();
            ifs.close();
			}
			else
			{
				string content = "Could not open file " + filename;
				response << "HTTP/1.1 400 Bad Request\r\nContent-Length: " << content.length() << "\r\n\r\n" << content;
			}


		};
		server.start();
}