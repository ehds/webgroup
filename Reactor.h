#ifndef WEB_REACTOR_H
#define WEB_REACTOR_H

#include "Epoller.h"
#include <string>
#include <list>
#include <unordered_map>
#include <queue>
#include <mutex>
class MasterReactor
{
public:
    MasterReactor() = default;
    MasterReactor(uint16_t port):port_(port){}
    MasterReactor (const MasterReactor&) =default;
    void setNonBlock(int fd);
    void UpdateEvent(int fd, int events, int op);
    void run();
    int init();
    void HandleAccept();
    void HandleWrite(int sockfd);
    void HandleRead(int fd);
    void HandleRedirect(int sockfd);
    int RegisterServer(std::string hostname,uint16_t port);
    struct Con {
        std::string readed;
        size_t written;
        bool writeEnabled;
        std::string response;
        Con() : written(0), writeEnabled(false) {}
    };
    std::unordered_map<int, Con> cons;

private:
    std::string httpRes = "HTTP/1.1 302 OK\r\nConnection: close\r\nCache-control: private\r\nLocation:http://";
    int serverfd_;
    uint16_t port_;
    Epoller epoll_;
    Epoller::Event ev_[1024];
    //路由表 是否存活
    std::unordered_map<std::string,bool> routers_;
    std::vector<std::string> activate_;
    int start;
    std::mutex mu_;
};

#endif //WEB_REACTOR_H
