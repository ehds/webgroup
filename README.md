# 互联网程序设计作业 
## 安装与运行
1. 编译
`mkdir bld && cd bld && cmake ..`

2. 运行server 
`./web 8000`  `./web 8001` `./web 8002`

3. 启动master
`./master`

## 测试
打开浏览器 http://127.0.0.1:3000
