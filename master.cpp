//
// Created by ehds on 6/2/19.
//
#include "Reactor.h"
#include "timercpp.h"
int main(int argc, char *argv[]){
    MasterReactor m(3000);

    m.init();
    m.RegisterServer("127.0.0.1",8000);
    m.RegisterServer("127.0.0.1",8001);
    m.RegisterServer("127.0.0.1",8002);

    m.run();

    return 0;
}