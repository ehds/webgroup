#include "Reactor.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "memory.h"
#include <fcntl.h>
#include <iostream>
#include <sstream>
void MasterReactor::setNonBlock(int fd){

int flags = fcntl(fd, F_GETFL, 0);
int r = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

void MasterReactor::UpdateEvent(int fd, int events, int op) {


}
int MasterReactor::init() {
    //首先先创建epoll
    int status = epoll_.create();
    if (status<0){
        return status;
    }
    start=0;

    //设置serversocket
    serverfd_ = socket(AF_INET, SOCK_STREAM, 0);

    //设置server的相关配置
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof addr);
    addr.sin_family = AF_INET;  //ipv4
    addr.sin_port = htons(port_);  // 设置端口
    addr.sin_addr.s_addr = INADDR_ANY; //地址  任意地址 0.0.0.0

    //设置地址为重用
    int reuse = 1;
    setsockopt(serverfd_, SOL_SOCKET, SO_REUSEADDR,(const void *)&reuse , sizeof(int));
    //开始监听
    int r = ::bind(serverfd_, (struct sockaddr *) &addr, sizeof(struct sockaddr));
    if(r<0)
        return r;
    if((r = listen(serverfd_, 20)<0)) return r;
    setNonBlock(serverfd_);

    //将serverfd_的enevnt放入epoll中
    Epoller::Event event(EPOLLIN, nullptr,serverfd_);
    epoll_.registerEvent(serverfd_,event);

}

void MasterReactor::run() {
   printf("run");
   for (;;) {
        int nr = epoll_.waitEvent(ev_, 1024, -1);
        for (int i = 0; i < nr; i++) {
            int fd = ev_[i].data.fd;
            int events = ev_[i].events;
            if (events & (EPOLLIN | EPOLLERR)) {
                if (fd == serverfd_) {
                    HandleAccept();
                } else {
                    HandleRead(fd);
                }
            }else if (events & EPOLLOUT) {
               printf("handling epollout\n");
                HandleWrite(fd);

            }else{
                continue;
            }
        }

    }
}

void MasterReactor::HandleAccept(){
    printf("accept");
    struct sockaddr_in raddr;
    socklen_t rsz = sizeof(raddr);
    int cfd = accept(serverfd_, (struct sockaddr *) &raddr, &rsz);
    sockaddr_in peer, local;
    socklen_t alen = sizeof(peer);
    int r = getpeername(cfd, (sockaddr *) &peer, &alen);
    printf("accept a connection from %s\n", inet_ntoa(raddr.sin_addr));
    setNonBlock(cfd);
    Epoller::Event env(EPOLLIN,nullptr,cfd);
    epoll_.registerEvent(cfd,env);

}

void MasterReactor::HandleRead(int fd){

    char buf[4096];
    int n = 0;
    while ((n = ::read(fd, buf, sizeof buf)) > 0) {
        printf("read %d bytes\n", n);
        std::string &readed = cons[fd].readed;
        readed.append(buf, n);
        if (readed.length() > 4) {
            if (readed.substr(readed.length() - 2, 2) == "\n\n" || readed.substr(readed.length() - 4, 4) == "\r\n\r\n") {
                //当读取到一个完整的http请求，测试发送响应
                HandleWrite(fd);
                printf("write");
            }
        }
    }
    if (n < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
        return;
    //实际应用中，n<0应当检查各类错误，如EINTR
 
    close(fd);
    epoll_.unregisterEvent(fd);
    cons.erase(fd);
}

void MasterReactor::HandleWrite(int sockfd) {

    Con &con = cons[sockfd];
    mu_.lock();
    std::string url = activate_[start++%activate_.size()];
    //while (activate_.empty()){

   // if(routers_[url]){
        printf("true");
        std::stringstream res ;
        res<<httpRes<<url<<"\r\n";
        con.response = res.str();
        printf("%s",con.response.data());

   // }
   // }
    mu_.unlock();

    if(!activate_.empty()) {
        size_t left = con.response.length() - con.written;
        int wd = 0;
        while ((wd = ::write(sockfd, con.response.data() + con.written, left)) > 0) {
            con.written += wd;
            left -= wd;
            printf("write %d bytes left: %lu\n", wd, left);
        };
        if (left == 0) {
            //        close(fd); // 测试中使用了keepalive，因此不关闭连接。连接会在read事件中关闭
            if (con.writeEnabled) {
                Epoller::Event env(EPOLLIN, nullptr, sockfd);
                epoll_.modifyEvent(sockfd, env);
                con.writeEnabled = false;
            }
            epoll_.unregisterEvent(sockfd);
            close(sockfd);
           cons.erase(sockfd);
            return;
        }
        if (wd < 0 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
            if (!con.writeEnabled) {
                Epoller::Event event(EPOLLIN | EPOLLOUT, nullptr, sockfd);
                epoll_.modifyEvent(sockfd, event);
                con.writeEnabled = true;
            }
            return;
        }
        if (wd <= 0) {
            printf("write error for %d: %d %s\n", sockfd, errno, strerror(errno));
            close(sockfd);
            //epoll_.unregisterEvent(sockfd);
            cons.erase(sockfd);
        }
    }
}

int MasterReactor::RegisterServer(std::string hostname, uint16_t port) {
    std::unique_lock<std::mutex> l(mu_);
    std::ostringstream s;
    s<<hostname<<":"<<std::to_string(port);
    auto ss = s.str();
    routers_[ss] = true;
    activate_.push_back(ss);
}
