#include "server_http.hpp"
#include "handle.hpp"
using namespace MyWeb;
int main(int argc, char *argv[]){
    int port = atoi(argv[1]);
    printf("%d",port);
	Server<HTTP> server(port, 4);
	start_server<Server<HTTP>>(server);
	return 0;
}